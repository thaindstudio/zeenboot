FROM ubuntu:18.04
RUN apt update
RUN apt upgrade -y
RUN apt install -y git npm curl
# RUN ln -s /usr/bin/nodejs /usr/bin/node
RUN npm install -g n
RUN n 10.23.2
RUN git clone https://github.com/deviavir/zenbot.git /zenbot
WORKDIR /zenbot
RUN cd /zenbot
RUN npm install 
RUN cp conf-sample.js conf.js
RUN apt install nano
RUN apt install build-essential mongodb -y
RUN mkdir /data/db -p
RUN rm -R /zenbot/.git
RUN apt purge -y git
RUn apt autoremove -y
RUN apt clean

CMD mongod 